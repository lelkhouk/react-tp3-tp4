import React, {useState} from 'react';
import data from '../api/db.json';

import {Navigate, useNavigate} from 'react-router-dom';

const AddReview = (props) => {
    const { user } = props;

    const reviews = data.reviews;
    const categories = data.categories;
    //console.log(reviews);
    const [valeurAuthor, setValeurAuthor] = useState('');
    const [valeurTitle, setValeurTitle] = useState('');
    const [valeurWine, setValeurWine] = useState('');
    const [valeurPrice, setValeurPrice] = useState('');
    const [valeurReview, setValeurReview] = useState('');
    const [valeurCategory, setValeurCategory] = useState('');

    const navigate = useNavigate();

    const handleSelectChange = (event) => {
        setValeurCategory(event.target.value);
    };
    const setTitle = (event) => {
        setValeurTitle(event.target.value);
    };
    const setWine = (event) => {
        setValeurWine(event.target.value);
    };
    const setPrice = (event) => {
        setValeurPrice(event.target.value);
    };

    const setReview = (event) => {
        setValeurReview(event.target.value);
    };

    const setAuthor = (event) => {
        setValeurAuthor(event.target.value);
    };

    let newReview = {taster_name:valeurAuthor, title:valeurTitle,category:valeurCategory, description:valeurReview,designation:valeurWine,price:valeurPrice,slug:valeurWine }
    const addReview = (e)=>{
        e.preventDefault();
        reviews.push(newReview);
        navigate(`/review/${newReview.slug}`);
        //console.log(newReview);
        //console.log(reviews);
    }


    
    console.log(categories);
    return (<div>
                {!user && (<Navigate to="/login" replace/>)}
                <h1>Add review</h1>
                <form onSubmit={addReview}>
                    <label for="author">Author :</label>
                    <input value={valeurAuthor} onChange={setAuthor} type="text" id="author" name="author" required></input>

                    <label for="title">Title :</label>
                    <input value={valeurTitle} onChange={setTitle} type="text" id="title" name="title" required></input>

                    <label for="wine">Wine (slug):</label>
                    <input value={valeurWine} onChange={setWine} type="text" id="wine" name="wine" required></input>

                    <label for="price">Price :</label>
                    <input value={valeurPrice} onChange={setPrice} type="number" id="price" name="price" required></input>

                    <label for="review">Review :</label>
                    <input value={valeurReview} onChange={setReview} type="text" id="review" name="review" required></input>
                    
                        <select value={valeurCategory} onChange={handleSelectChange} id="categories" name="categories" required>
                            {categories.map((cat,index)=>{
                                return(<option key={index} value={cat.key}>{cat.label}</option>)
                            })}
                        </select>
                        <input  type="submit" value="Envoyer"/>
                </form >
            </div>
    );
};

export default AddReview;