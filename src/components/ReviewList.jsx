import React, { useState } from 'react';
import data from '../api/db.json';
import { Link, Navigate, useNavigate, useParams } from 'react-router-dom';

const ReviewList = (props) =>{
    const {user} = props ;
    const [db, setDb] = useState(data.reviews);
    //console.log(db);
    const navigate = useNavigate();

    // supprimer une review (par un admin par exemple)
    const deleteReview = (e)=>{
        const slugReview = e.target.value;
        setDb(db.filter((review)=>review.slug !== slugReview))
    }
    const categorie = data.categories;
    //console.log(categorie);
    // État local pour stocker la valeur sélectionnée
    const [valeurSelectionnee, setValeurSelectionnee] = useState('All');

    const params = useParams();

    console.log('test '+params.cat);


    const handleSelectChange = (e) => {
        // Met à jour l'état avec la nouvelle valeur sélectionnée
        setValeurSelectionnee(e.target.value);
        console.log(e.target.value);
        navigate(`/${e.target.value}`);
    };

    const filtreByName = () => {
        const sortedArray = [...db];
        sortedArray.sort( (review1,review2)=>review1.title.localeCompare(review2.title) )
        setDb(sortedArray);
        console.log(sortedArray)
        
    }
    const filtreByPoint = () => {
        const sortedArray = [...db];
        sortedArray.sort( (review1,review2)=>review2.points - review1.points)
        setDb(sortedArray);
        console.log(sortedArray)
    }
    return (
    <section data-name="review-list">
                <div className="">
                    <h4>Categories</h4>
                    <p>
                        <select value={valeurSelectionnee} onChange={handleSelectChange}>
                            <option value="All">All</option>
                            {categorie.map((cat)=>{
                                return (
                                <option value={`${cat.key}`}>{cat.label}</option>)
                                })
                            }
                        </select>
                    </p>
                    <div> filtres 
                        <button onClick={filtreByName}>Nom</button>
                        <button onClick={filtreByPoint}>Mieux noté</button>
                    </div>
                </div>
                <grid>
                    { 
                        db.map((oneReview,index) => {
                            if(
                                valeurSelectionnee==='All'
                                || valeurSelectionnee===oneReview.category 
                            ){
                                return (<div col="1/2" key={index}>
                                    <card>
                                            <h5>{oneReview.title}</h5>
                                            <p> {oneReview.points} / 100</p>
                                            <p> {oneReview.category}</p>

                                            {user && (<button onClick={deleteReview} value={oneReview.slug}>Delete review</button>)}
                                            <Link to={`/review/${oneReview.slug}`}>
                                                <button>About Review</button>
                                            </Link>
                                    </card>
                                </div>)
                            }
                            return <></>
                        })
                    }
                </grid>
            </section>
            )

};

export default ReviewList;