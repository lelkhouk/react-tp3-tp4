import logo from '../assets/logo.png';
import { Link } from 'react-router-dom';
const Header = (props)=>{
    const {user} = props;
    
    // Seconde solution plus propre
    //
    // let LoginLogout = <></>;
    // if(user) {
    //     LoginLogout = (
    //         <li>
    //             <p>vous etes connecté en tant que {user?.username}</p>
    //             <Link to="/logout">Logout</Link>
    //         </li>
    //     )
    // } else {
    //     LoginLogout = (
    //         <li>
    //             <p>vous etes connecté en tant que {user?.username}</p>
    //             <Link to="/logout">Logout</Link>
    //         </li>
    //     )
    // }


    return (
        <nav fx="">
            <header>
                <Link to="/"><img src={logo} alt="React-Wines logo" /></Link>
            </header>
            <div>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/review/random">Random</Link>
                    </li>
                    <li>
                        <Link to="/add">Add review</Link>
                    </li>
                    
                    {/* Seconde solution plus propre {LoginLogout} */}
                    {
                        user===null ? // if(user===null)
                            (<li>
                                <Link to="/login">Login</Link>
                            </li>)
                        : // else
                            (
                                <li>
                                    <p>vous etes connecté en tant que {user?.username}</p>
                                    <Link to="/logout">Logout</Link>
                                </li>
                            )
                    }
                    
                </ul>
            </div>
        </nav>
    )
};

export default Header;