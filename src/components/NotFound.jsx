import { Link } from 'react-router-dom';


const NotFound = ()=>{
    return(
        <>
            <h1>Error 404 </h1>
            <p> return to the <Link to="/">Homepage</Link> </p>
        </>
    )

};

export default NotFound;