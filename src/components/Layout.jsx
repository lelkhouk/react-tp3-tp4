import Header from './Header';
import Footer from './Footer';

const Layout = (props)=>{
    const {children, user} = props;
    return(
        <>
            <Header user={user}/>
                <main style={{ margin: '6rem' }}>
                    {children}
                </main>
            <Footer/>
        </>
    )

};

export default Layout;