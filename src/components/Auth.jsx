import React, {useState} from 'react';
import { useNavigate, Navigate, useLocation } from 'react-router-dom';


// props = { onLogin : function }
const Login = (props) => {
    const { onLogin, user} = props;

    const navigate = useNavigate();
    const location = useLocation()

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const setValeurUsername = (event) => {
        setUsername(event.target.value);
    };
    const setValeurPassword = (event) => {
        setPassword(event.target.value);
    };

    const onSubmitLogin = (e) => {
        e.preventDefault()
        onLogin(username, password)
        navigate('/')
    }
    /*
    let islogged = false;
    const islogin = () =>{
        islogged = true;
        console.log(username+' est connecté !');
    }*/
    return(
        <>
            {user && (<Navigate to='/' replace/>)}
            <h1>Login</h1>
            <form onSubmit={onSubmitLogin} >
                <label for="username">Nom d'utilisateur :</label>
                <input value={username} onChange={setValeurUsername} type="text" id="username" name="username" required/>

                <label for="password">Mot de passe :</label>
                <input value={password} onChange={setValeurPassword} type="password" id="password" name="password" required/>
                <button type="submit">Se Connecter</button>
            </form>
        </>
    )

};


// props = { onLogout: function }
const Logout = (props) => {
    const { onLogout, user } = props;

    const navigate = useNavigate();

    const onSubmitLogout = (e) => {
        e.preventDefault()
        onLogout()
        navigate('/')
    }

    return (
        <>
            {/* si on a user est falsy alors on fait un rendu du Navigate */}
            {!user && (<Navigate
                to="/login"
                state={{
                    from:'/logout'
                }}
                replace
            />)}
            <h1>Déconnexion</h1>
            <form onSubmit={onSubmitLogout} >
                <p>Êtes-vous sûr de vouloir vous déconnecter ?</p>
                <button type="submit">Se Déconnecter</button>
            </form>
        </>
    );
};

export {
    Login,
    Logout
};