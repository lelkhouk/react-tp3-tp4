import React, {useState} from 'react'
import ReviewList from './ReviewList'
import Layout from './Layout'
import { Login, Logout } from './Auth'
import NotFound from './NotFound'
import AddReview from './AddReview'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Review from './Review'

const App = () => {

    const [user, setUser] = useState(null);
    
    const handleLogin = (username, password) => {
        const userToSave = {
            username: username,
            password: password
        }
        setUser(userToSave)
    }

    const handleLogout = () => {
        setUser(null)
    }

    return <>
        <BrowserRouter>
            <Layout user={user}>
                <Routes>
                    <Route path="/" element={<ReviewList user={user}/>} />
                    <Route path="/:cat" element={<ReviewList user={user}/>} />
                    <Route path="review/:slug" element={<Review />} />
                    <Route path="add" element={<AddReview user={user} />} />
                    <Route path="login" element={<Login onLogin={handleLogin} user={user} />} />
                    <Route path="logout" element={<Logout onLogout={handleLogout} user={user} />} />                      
                    <Route path="*" element={<NotFound/>} />
                </Routes>
            </Layout>
            
        </BrowserRouter>
    </>
}

export default App