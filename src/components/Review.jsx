import React from 'react';
import data from '../api/db.json';
import { useParams } from 'react-router-dom'
import NotFound from './NotFound';
const Review = () => {
    const params = useParams();
    let oneReview;



    if(params.slug === "random") {
        // si params === "random" -> récupérer aléatoirement
        // on récupère in index aléatoire
        const randomIndex = Math.ceil(Math.random() * data.reviews.length);
        // en suite on find id suivant randomIndex
        oneReview = data.reviews[randomIndex];
    } else {
        // sinon on cherche suivant le slug donné
        oneReview = data.reviews.find((oneReview)=> oneReview.slug === params.slug);
    }
    if(!oneReview) {
        return (<NotFound></NotFound>)
    }
    return (
        <>
            {<section data-name="review">
            <h2>Nouveau</h2>
            <h3>{oneReview.title}</h3>
            <p>
                Price : <b>{oneReview.price} $</b>
            </p>
            <blockquote>
                <p>
                {oneReview.description}
                </p>
                <p txt="r">
                    <i>- {oneReview.taster_name}</i>
                </p>
            </blockquote>
        </section>
        }
        </>
    )
};

export default Review;